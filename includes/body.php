<div class="container">
    <div class="row flex-lg-nowrap">
        <div class="col">
            <div class="row flex-lg-nowrap">
                <div class="col mb-3">
                    <div class="e-panel card">
                        <div class="card-body">
                            <div class="card-title">
                                <h6 class="mr-2"><span>Users</span></h6>
                            </div>
                        </div>
                    </div>
                    <div class="e-table">

                        <div class="table-responsive table-lg mt-3">
                            <!--Control block-->
                            <div class="btn-group mb-3" data-toggle="buttons">
                                <button class="btn btn-secondary add-user" data-bs-title="Adding a user"
                                        type="button" data-bs-toggle="modal"
                                        data-bs-target="#add-user">
                                    <i class="fa fa-plus"></i> Add user
                                </button>
                                <select data-coor="up" class="btn btn-secondary action">
                                    <?php include 'select-body.php' ?>
                                </select>
                                <button data-coor="up" class="btn btn-secondary ok">Ok</button>
                            </div>
                            <?php include_once 'model-form-add-user.php'?>
                            <?php include_once 'model-form-delete-user.php'?>

                            <table class="table table-bordered">
                                <thead>
                                <!--model error-->
                                <div id="myModal" class="modal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div id="msg" class="modal-body bg-danger text-white text-center">
                                                <!--...-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end model error-->
                                <tr>
                                    <th class="align-top">
                                        <div class="custom-control custom-control-inline custom-checkbox custom-control-nameless m-0">
                                            <input type="checkbox" class="custom-control-input" id="checkAll">
                                            <label class="custom-control-label" for="checkAll"></label>
                                        </div>
                                    </th>
                                    <th class="max-width">Name</th>
                                    <th class="sortable">Role</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody id="users">
                                <!--users-->
                                </tbody>
                            </table>
                            <!--Control block-->
                            <div class="btn-group mb-3" data-toggle="buttons">
                                <button class="btn btn-secondary add-user" data-bs-title="Adding a user"
                                        type="button"
                                        data-bs-target="#add-user"
                                        data-bs-toggle="modal">
                                    <i class="fa fa-plus"></i> Add user
                                </button>
                                <select data-coor="down" class="btn btn-secondary action">
                                    <?php include 'select-body.php' ?>
                                </select>
                                <button data-coor="down" class="btn btn-secondary ok">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
