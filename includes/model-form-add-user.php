<div class="modal fade" id="add-user" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!--   error massage-->
            <div id="error" class="bg-danger text-white text-center"></div>
            <div class="modal-body">
                <form id="form">
                    <div class="mb-2">
                        <label for="first-name" class="col-form-label">First name</label>
                        <input class="form-control" type="text" name="first_name" id="first-name">
                    </div>
                    <div class="mb-2">
                        <label for="last-name" class="col-form-label">Last name</label>
                        <input class="form-control" type="text" name="last_name" id="last-name">
                    </div>
                    <div class="mb-2">
                        <label for="status" class="checkbox-google">
                            <input type="checkbox" name="status" id="status">
                            <span class="checkbox-google-switch"></span>
                            active or not active
                        </label>
                    </div>
                    <div class="mb-2">
                        <div><label for="role" class="col-form-label">Role</label></div>
                        <select id="role" name="role" class="btn btn-secondary role">
                            <option value="0" selected>please role user</option>
                            <option value="1">user</option>
                            <option value="2">admin</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!---->
            </div>
        </div>
    </div>
</div>
