<div class="modal fade" id="del-user" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title del" id="exampleModalLabel">New message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!--   error massage-->
            <div id="error" class="bg-danger text-white text-center"></div>
            <div class="modal-body" id="form-del"></div>
            <div class="modal-footer form-del"></div>
        </div>
    </div>
</div>
