<?php
require_once '../config/db.php';

if (empty($_GET['id'])){
    $error = "Not found user";

    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo(json_encode($answer));
    return;
}


$id = $_GET['id'];
$error = null;
$sql = "SELECT * FROM `users` WHERE id = $id";

$user = mysqli_query($connect, $sql);

if (!$user = $user->fetch_assoc()) {
    $error = "not found user";

    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
        'id' => $id,
    ];
    echo(json_encode($answer));
    return;

} else {
    $answer = [
        'status' => true,
        'error' => $error,
        'user' => $user,
    ];
}
echo json_encode($answer);
