<?php
require_once '../config/db.php';

$error = null;
if ((empty($_POST['first_name']) or (empty($_POST['last_name'])) or (empty($_POST['role'])))) {
    $error = 'Error, not all fields are filled';
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error,
        ]
    ];

    echo(json_encode($answer));
    return;
} else {
    $data = [
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'role' => $_POST['role'],
        'status' => $_POST['status'],
    ];

    $sql = "INSERT INTO `users` (`id`,`first_name`, `last_name`, `role`,`status` ) VALUES (NULL ,'{$data['first_name']}', '{$data['last_name']}','{$data['role']}','{$data['status']}')";

    $userLast = "";
    if (!$connect->query($sql)) {
        $error = "Error, user don't added";
    } else {
        $sql2 = "SELECT * FROM users ORDER BY id DESC LIMIT 1";
        $userLast = $connect->query($sql2)->fetch_assoc();
    }

    if ($error == null) {
        $answer = [
            'status' => true,
            'error' => $error,
            'user' => $userLast,
        ];
    } else {
        $answer = [
            'status' => false,
            'error' => [
                'code' => 100,
                'message' => $error
            ],
        ];
    }
    echo(json_encode($answer));
}


