<?php
require_once '../config/db.php';

$json_data = [];
$query = "SELECT * FROM `users`";

$users = $connect->query($query);

while ($row = $users->fetch_assoc()) {
    $json_data[] = $row;
}

echo json_encode($json_data);
