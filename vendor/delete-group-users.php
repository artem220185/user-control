<?php
require_once '../config/db.php';
$error = null;


if (empty($_POST['action']) || $_POST['action'] == "0") {

    $error = "No action selected!";
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo(json_encode($answer));
    return;
}

    if (empty($_POST['ids'])) {
        $error = "No users selected!";
        $answer = [
            'status' => false,
            'error' => [
                'code' => 100,
                'message' => $error
            ],
        ];
        echo(json_encode($answer));
        return;
    }

    $action = $_POST['action'];

    $ids = trim($_POST['ids'], ',');
    $ids = explode(',', $ids);


    $editedIds = [];  // id которе есть в базе

    foreach ($ids as $id) {
        $sql = "SELECT * FROM `users` WHERE id = $id";

        $user = mysqli_query($connect, $sql);

        if ($user->fetch_assoc()) {
            mysqli_query($connect, "DELETE FROM `users` WHERE `users`.`id` = '$id'");
            array_push($editedIds, $id);
        } else {
            $error = "not found users";
        }
    }

    if ($error == null) {
        $answer = [
            'status' => true,
            'error' => $error,
            'ids' => $editedIds,
        ];

    } else {
        $answer = [
            'status' => false,
            'error' => [
                'code' => 100,
                'message' => $error,
            ],
            'ids' => $editedIds,
        ];
    }
    echo(json_encode($answer));