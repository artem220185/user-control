<?php
require_once '../config/db.php';

$error = null;


if (!$_POST['id']) {
    $error = "not found user";

    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo json_encode($answer);
    return;
}

//    проверка на пустое поле
foreach ($_POST as $item) {
    if ($item == "") {
        $error = "empty field";

        $answer = [
            'status' => false,
            'error' => [
                'code' => 100,
                'message' => $error
            ],
            'id' => $_POST['id']
        ];
        echo json_encode($answer);
        return;
    }
}

$user = [
    'id' => $_POST['id'],
    'first_name' => $_POST['first_name'],
    'last_name' => $_POST['last_name'],
    'role' => $_POST['role'],
    'status' => $_POST['status'],
];

$sql = "UPDATE `users` SET `first_name` = '{$user['first_name']}', `last_name` = '{$user['last_name']}', `role` = '{$user['role']}' , `status` = '{$user['status']}' WHERE `users`.`id` = '{$user['id']}'";

if (!mysqli_query($connect, $sql)) {
    $error = "error when changing user";
}

if ($error == null) {
    $answer = [
        'status' => true,
        'error' => $error,
        'user' => $user,
    ];
} else {
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
        'id' => $user['id'],
    ];
}
echo json_encode($answer);
