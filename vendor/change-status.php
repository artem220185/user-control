<?php
require_once '../config/db.php';

$error = null;

if (empty($_POST['action'])) {
    $error = "No action selected!";
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo(json_encode($answer));
    return;
}

if (empty($_POST['ids'])) {
    $error = "No users selected!";
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo(json_encode($answer));
    return;
}

$action = $_POST['action'];

$ids = trim($_POST['ids'], ',');
$ids = explode(',', $ids);

if ($action == 'active') {
    $status = 1;
} else {
    $status = 0;
}
$editIds = [];
foreach ($ids as $id) {
    $sql = "SELECT * FROM `users` WHERE id = $id";

    $user = mysqli_query($connect, $sql);

    if ($user->fetch_assoc()) {
        mysqli_query($connect, "UPDATE `users` SET `status` = '$status' WHERE `users`.`id` = '$id'");
        array_push($editIds, $id);
    } else {
        $error = "not found users";
    }
}

if ($error == null) {
    $answer = [
        'status' => true,
        'error' => $error,
        'ids' => $editIds,
    ];

} else {
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error,
        ],
        'ids' => $editIds,
    ];
}
echo(json_encode($answer));
