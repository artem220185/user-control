<?php
require_once '../config/db.php';

$error = null;
if (empty($_POST['id'])){
    $error = "Not found user";

    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error
        ],
    ];
    echo(json_encode($answer));
    return;
}


$id = $_POST['id'];

$query = "DELETE FROM `users` WHERE `users`.`id` = $id";

if (!mysqli_query($connect, $query)) {
    $error = "not found user";
}

if ($error == null) {
    $answer = [
        'status' => true,
        'error' => $error,
        'id' => $id,
    ];
} else {
    $answer = [
        'status' => false,
        'error' => [
            'code' => 100,
            'message' => $error,
        ],
        'id' => $id,
    ];
}

echo(json_encode($answer));
