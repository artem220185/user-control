$(document).ready(function () {
    getFormAdd()
    addUser()
    getUserForDel()
    delUser()
    getUserForUpdate()
    updateUsers()
    //показать всех
    showUsers()
})

function showUsers() {
    $.ajax({
        url: "vendor/all-users.php",
        success: function (result) {
            let temp = JSON.parse(result)
            $('#users').empty();
            for (let i = 0; i < temp.length; i++) {
                $('#users').append(showUser(temp[i]))
            }
            //запуск всех форм
            ch()
        }
    })
}

//удаление пользователя
function delUser() {
    $(document).on('click', 'button.del', function () {
        let id = $(this).attr('id')

        $.ajax({
            type: "POST",
            url: "vendor/delete-user.php",
            data: {id: id},
            success: function (result) {

                let answer = JSON.parse(result)
                if (!answer.status) {
                    $("#myModal").show('slow', function () {
                        $('#error').html(answer.error.message)
                    })
                    setTimeout(function () {
                        $("#myModal").hide('slow');
                    }, 2000);
                } else {
                    $(`#user-${id}`).remove();
                }
            },
        })
    })
}

function BeforeBulkDeletion(idS, action) {
    if (idS !== "" && action !== "" && action !== "0") {
        $('h5.del').html('Deleting multiple users')
        $('#form-del').html('Are you sure you want to delete users?')
        $('.form-del').html(`<button type="button" class="btn btn-danger del-users" data-bs-dismiss="modal">Delete</button>
                             <button type="button" class="btn btn-secondary cancel-del" data-bs-dismiss="modal">Cancel</button>`)
        $('#del-user').modal('show')

        $('button.del-users').on('click', function () {
            delUserS(idS, action)
        })
        $('button.cancel-del').on('click', function () {
            $('input.ch:checkbox').prop('checked', false); //обнуление чекбоксов
            $('#checkAll').prop('checked', false)
            $('select.action').val('0')
        })
    } else {
        delUserS(idS, action)
    }
}

//групповое удаление
function delUserS(idS, action) {
    $.ajax({
        type: "POST",
        url: "vendor/delete-group-users.php",
        data: {ids: idS, action: action},
        success: function (result) {
            let answer = JSON.parse(result)
            if (!answer.status) {
                $('#msg').html(answer.error.message)
                $('#myModal').show('show')
                setTimeout(function () {
                    $("#myModal").hide('slow')
                }, 2000);
            } else {
                answer.ids.forEach(function (item) {
                    $(`#user-${item}`).remove()
                    $('select.action').val('') //обнуление селекторов
                })
            }
        },
    })
}

// вставка формы добавления
function getFormAdd() {
    $(document).on('click', '.add-user', function () {
        $("#error").hide('slow')
        $('#add-user').modal('show')

        $('input.first_name').val('')
        $('input.last_name').val('')
        $('#role option:first').prop('selected', true)
        $('input.status').prop('checked', false)

        let title = $(this).attr('data-bs-title')
        $('h5.modal-title').html(title)
        // $('#form').html(htmlModelAdd())
        $('.modal-footer').html(`<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary submit">Add</button>`)
    })
}

//редоринг мод. формы создание пользователя
function htmlModelAdd() {
    return `<div class="mb-2">
                <label for="first-name" class="col-form-label">First name</label>
                <input class="form-control" type="text" name="first_name" id="first-name">
            </div>
            <div class="mb-2">
                <label for="last-name" class="col-form-label">Last name</label>
                <input class="form-control" type="text" name="last_name" id="last-name">
            </div>
            <div class="mb-2">
                <label for="status" class="checkbox-google">
                    <input type="checkbox" name="status" id="status">
                    <span class="checkbox-google-switch"></span>
                    active or not active
                </label>
            </div>
            <div class="mb-2">
               <div><label for="role" class="col-form-label">Role</label></div>
                <select id="role" name="role" class="btn btn-secondary role">
                    <option value="" selected>please role user</option>
                    <option value="1">user</option>
                    <option value="2">admin</option>
                </select>
            </div>`
}

// добавление пользователя
function addUser() {
    $(document).on('click', 'button.submit', function () {
        let firstName = $('#first-name').val()
        let lastName = $('#last-name').val()
        let role = $('#role').val()
        let status = $('#status').is(':checked') ? 1 : 0

        $.ajax({
            method: "POST",
            url: "vendor/add.php",
            data: {first_name: firstName, last_name: lastName, role: role, status: status},
            success: function (result) {
                let answer = JSON.parse(result)

                if (!answer.status) {
                    $('#error').html(answer.error.message)
                    $("#error").show('slow')
                } else {
                    $('#add-user').modal('hide')
                    $('#users').append(showUser(answer.user))
                }
            }
        })

        // $('input.first_name').val('')
        // $('input.last_name').val('')
        // $('#role').val('0').is('selected')
        // $('select.role').val(0).is(':selected')
        // $('input.status').prop('checked', false)
    })
}

//показать пользователя по id
function getUserForUpdate() {
    $(document).on('click', '.get-user-for-update', function () {
        $("#error").hide('slow')

        let id = $(this).attr('data-bs-id')
        let title = $(this).attr('data-bs-title')
        $('h5.modal-title').html(title)

        $.ajax({
            type: "GET",
            url: "vendor/get-user.php",
            data: {id: id},
            success: function (result) {
                let answer = JSON.parse(result)
                if (!answer.status) {
                    $('#msg').html(answer.error.message)
                    $('#myModal').show('show')
                    setTimeout(function () {
                        $("#myModal").hide('slow')
                    }, 2000)
                } else {
                    $('#form').html(htmlModelUpdateUser(answer.user))


                    $('#first-name').val(answer.user.first_name)
                    $('#last-name').val(answer.user.last_name)
                    $('label.checkbox-google').attr('for', `status-${answer.user.id}`)
                    $('input.status').attr('id', `status-${answer.user.id}`)
                    if (answer.user.status == 1) {
                        $('input.status').prop('checked', true)
                    } else {
                        $('input.status').prop('checked', false)
                    }


                    // ------------------
                    $('.modal-footer').html(`<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                             <button type="button" id="save-${answer.user.id}" class="btn btn-primary update" data-bs-dismiss="modal">Save</button>`)
                    $('#add-user').modal('show')
                }
            }
        })
    })
}

//заполнение формы для изминения пользователя
function htmlModelUpdateUser(user) {
    let isActive = '';
    if (user.status == 1) {
        isActive = 'checked';
    }
    // const isUser = user.role == '1' ? 'selected' : '';
    // const isAdmin = user.role == '2' ? 'selected' : '';

    return `<div class="form-group mb-2">
                    <label for="first-name" class="col-form-label">First name</label>
                    <input id="first-name" class="form-control first_name" type="text" name="first_name">
                </div>
                <div class="form-group mb-2">
                    <label for="last-name" class="col-form-label">Last name</label>
                    <input id="last-name" class="form-control last_name" type="text" name="last_name">
                </div>
                <label class="col-form-label">Active or not active</label>
                <div class="form-group mb-2">
                    <label for="" class="checkbox-google">
                        <input type="checkbox" class="status" name="status">
                        <span class="checkbox-google-switch"></span>
                    </label>
                </div>
                <label for="role" class="col-form-label">Role</label>
                <div class="form-group mb-4">
                    <select id="role" name="role" class="btn btn-secondary role">
                        <option value="1" ${isUser}>user</option>
                        <option value="2" ${isAdmin}>admin</option>
                    </select>
                </div>
            </div>`
}

//сообщение-форма перед удалением
function getUserForDel() {
    $(document).on('click', 'button.get-user-for-del', function () {
        $("#error").hide('slow')

        let id = $(this).attr('data-bs-id')
        let name = $(this).attr('data-bs-name')

        $.ajax({
            type: "GET",
            url: "vendor/get-user.php",
            data: {id: id},
            success: function (result) {
                let answer = JSON.parse(result)
                if (!answer.status) {
                    $('#msg').html(answer.error.message)
                    $('#myModal').show('show')
                    setTimeout(function () {
                        $("#myModal").hide('slow')
                    }, 2000)
                } else {
                    $('h5.modal-title').html('Deleting a user')
                    $('#form').html(htmlModelDel(name))
                    $('.modal-footer').html(`<button id="${id}" type="button" class="btn btn-danger del" data-bs-dismiss="modal">Delete</button>
                                             <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>`)
                    $('#add-user').modal('show')
                }
            }
        })
    })
}

//заполненни конфирм-окна перед удаленим
function htmlModelDel(name) {
    return `<div class="modal-body">
                Are you sure you want to delete the user 
                <span style="font-weight: 700"> ${name}</span>
            </div>`
}

// изминеие пользователя
function updateUsers() {
    $(document).on('click', '.update', function () {
        let id = $(this).attr('id').replace('save-', '')
        let first_name = $('input.first_name').val()
        let last_name = $('input.last_name').val()
        let role = $('select.role').val()
        let status = $('input.status').is(':checked') ? 1 : 0

        $.ajax({
            type: "POST",
            url: "vendor/update-user.php",
            data: {id: id, first_name: first_name, last_name: last_name, role: role, status: status},
            success: function (result) {
                let answer = JSON.parse(result)

                if (!answer.status) {
                    $("#myModal").show('slow', function () {
                        $('#msg').html(answer.error.message)
                    })
                    setTimeout(function () {
                        $("#myModal").hide('slow')
                    }, 2000)
                } else {
                    $(`#user-${id}`).replaceWith(showUser(answer.user))
                }
            },
        })
    })
}

//отображение пользователя
function showUser(item) {
    let role = (item.role == 1) ? 'user' : 'admin'
    let isColor = 'base-color'

    if (item.status == 1) {
        isColor = 'active';
    }
    return `<tr id="user-${item.id}">
                <td class="align-middle">
                     <div class="custom-control custom-control-inline custom-checkbox custom-control-nameless m-0 align-top">
                         <input type="checkbox" class="custom-control-input ch" id="${item.id}">
                         <label for="${item.id}" class="custom-control-label"></label>
                     </div>
                </td>
                    <td class="text-nowrap align-middle">${item.first_name} ${item.last_name}</td>
                    <td class="text-nowrap align-middle"><span>${role} </span></td>
                    <td class="text-center align-middle">
                    <i id="color-${item.id}" class="fa fa-circle ${isColor}"></i>
                </td>
                <td class="text-center align-middle">
                     <div class="btn-group align-top">
                         <button class="btn btn-sm btn-outline-secondary get-user-for-update" type="button"
                                  data-bs-id="${item.id}" data-bs-title="Updating user">
                                 <i class="fa fa-edit"></i>
                         </button>
                         <button class="btn btn-sm btn-outline-danger get-user-for-del" type="button"
                                 data-bs-id="${item.id}"
                                 data-bs-name="${item.first_name} ${item.last_name}">
                                 <i class="fa fa-trash"></i>
                         </button>
                     </div>
                </td>
            </tr>`
}

//изминнени статуса группы пользователей
function statusEdit(ids, action) {
    $.ajax({
        type: "POST",
        url: "vendor/change-status.php",
        data: {ids: ids, action: action},

        success: function (result) {
            let answer = JSON.parse(result)

            if (!answer.status) {
                $('#msg').html(answer.error.message)
                $('#myModal').show('show')
                setTimeout(function () {
                    $("#myModal").hide('slow')
                }, 2000)
            } else {
                let isColor = 'base-color'

                answer.ids.forEach(function (item) {
                    if (item.status == 1) {
                        isColor = 'active'
                    }

                    $(`#color-${item}`).replaceWith(`<i id="color-${item}" class="fa fa-circle ${isColor}"></i>`)
                })
                $('input.ch:checkbox').prop('checked', false) //обнуление чекбоксов
                $('#checkAll').prop('checked', false)

                $('select.action').val('') //обнуление селекторов
            }
        },
    })
}

//====================управление чекбоксами====================================
function ch() {
    $(document).on('change', '#checkAll', function () {
        $('input.ch:checkbox').prop('checked', this.checked);
    });

    $(document).on('click', 'input.ch:checkbox', function () {
        let clicked = $('input.ch:checkbox:checked').length;
        let all = $('input.ch:checkbox').length;
        if (clicked == all) {
            $('#checkAll').prop('checked', true)
        } else {
            $('#checkAll').prop('checked', false)
        }
    })
}

// при клике на кнопу обработка чекбоксов
$(document).ready(function () {
    $(document).on('click', '.ok', function () {
        let btn = $(this).data('coor')
        let action = $(`select.action[data-coor=${btn}]`).val()

        let srtChecks = ""
        $('input.ch:checkbox:checked').each(function () {
            srtChecks += ($(this).attr('id')) + ","
        })

        switch (action) {
            // set active
            case '1':
                statusEdit(srtChecks, action)
                break;
            // set not active
            case '2':
                statusEdit(srtChecks, action)
                break;
            // '3': delete or please select
            default:
                BeforeBulkDeletion(srtChecks, action)
                break;
        }
    })
});
